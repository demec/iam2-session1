Course Cheatsheet
-----------------

# Bash
Helpful ~/.bashrc
-----------------
```
# append to the history file, don't overwrite it
shopt -s histappend

# Entries beginning with space aren't added into history, and duplicate
# entries will be erased (leaving the most recent entry).
# Lots o' history.
export HISTSIZE=10000
export HISTFILESIZE=10000

# leave some commands out of history log
export HISTIGNORE="&:??:[ ]*:clear:exit:logout"

export EDITOR='vim'

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
```

# Git
## Resources
[Git Cheatsheet](https://www.git-tower.com/blog/git-cheat-sheet/)

## Commands
```
# View current state of working area and staging
git status

# Make a directory a git repo
git init

# Make and checkout branch
git checkout -b new-branch-name

# Delete a branch that has been merged
git branch -d old-branch-name

# Stage all files in directory to git
git add .

# Commit all staged files
git commit -m "commit message"

# Update the master branch in the remote "origin" repository
git push origin master

# Checkout master and pull latest changes
git checkout master
git pull origin master
```

# Python
## Script Skeleton
```
#! /usr/bin/env python

def do_stuff():
    print("Hello world!")

if __name__ == "__main__":
    do_stuff()
```

Run the script with `python3 script.py`
## Unittesting

[Unittest library](https://docs.python.org/3/library/unittest.html)


```
import unittest
class TestMyCode(unittest.TestCase):
    def test_is_true(self):
        self.assertTrue(True)

```

### Unittesting commands
#### Install tools
`pip install --user flake8 pytest pytest-cov`
#### Check code formatting
`flake8`
#### Run tests
`py.test --cov=.`
#### Run tests and analyze coverage
`py.test --cov=.`

# Docker
List running containers
`docker ps`

List all containers
`docker ps --all`

Build a docker image
`docker build -t "Image name" .`

Run a container
`docker run -d -p <host port>:<container port>`

See application logs
`docker logs <container id>`

Stop a running container
`docker stop <container id>`

Remove a container
`docker rm <container id>`

Execute a command in a container
`docker exec <container id> <command>`

"Connect" to a container
`docker exec -it <container id> /bin/sh`

# AWS
## AWS CLI

### Configure/setup access to account
1. Copy/create access key in console
    * username in top right -> Security Credentials -> Create access key
    * Copy the Access key ID and secret
2. Run `aws configure` in terminal
3. Enter 'us-east-2' as the region and 'table' as the output 
4. Run ```
echo "complete -C aws_completer aws" >> ~/.bash_profile
source ~/.bash_profile
```

## List instance information
`aws ec2 describe-instances`

import sys

def calculate(num1, num2, op):
    """
    Calculate an operation on two passed in integers. 
    Valid operations are addition, subtraction, multiplication and division.
    """
    try:
        num1 = int(num1)
        num2 = int(num2)
        if op not in '+-*/':
            print('Bad operator:', op)
            return None
        if op == '+':
            return num1 + num2
        if op == '-':
            return num1 - num2
        if op == '*':
            return num1 * num2
        if op == '/':
            return num1 / num2
    except TypeError as oops:
        print('One or more arguments not a number.', oops)
    except Exception as oops:
        print('Something went wrong:', oops, type(oops))

if len(sys.argv) < 4:
    print(f'usage: {sys.argv[0]} num1 num2 operator')
else:
    print(calculate(sys.argv[1], sys.argv[2], sys.argv[3]))

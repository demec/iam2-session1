import math
import random
import sys
from multiprocessing.pool import Pool

def shuffle(deck):
    global orig_deck
    count = 1

    while True:
        index = random.randint(0, len(deck) - 1)
        deck[0], deck[index] = deck[index], deck[0]
        if deck == orig_deck:
            break
        count += 1

    return count

if __name__ == '__main__':
    # Grab size of deck from command line
    size_of_deck = int(sys.argv[1])
    orig_deck = list(range(1, size_of_deck + 1))

    total_count = 0
    iterations = 10_000
    pool = Pool(processes=4)

    iters = [pool.apply_async(shuffle, args=(list(orig_deck),))
			for _ in range(iterations)]
    out = [p.get() for p in iters]

    print(f'{sum(out) / iterations:.2f} {math.factorial(size_of_deck)}')

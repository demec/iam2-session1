#!/bin/bash

#####
#course notes
#####
#-x above would debug
#set -x and set +x around a specific block to debug
#####

#simple note-taking script
#plurlasight course

#get the date
date=$(date)

#get the topic
topic="$1"

#set the filename
filename=~/iam2-session1/week3/${topic}notes.txt
#or "${HOME}/${topic}notes.txt"
#or ~/...../"$topic notes.txt"

#ask for user input
read -p "Enter a note: " note

if [[ $note ]]; then
	echo $date: $note >> "$filename"
	echo Note \'$note\' saved to $filename
else
	echo "No input; note not saved"
fi

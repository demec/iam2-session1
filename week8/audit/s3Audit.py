"""S3 Auditing Script 
aws lambda invoke --invocation-type Event --function-name <YOUR LAMBDA) /tmp/lambda-outfile 
"""

import boto3
import requests
import logging

logging.basicConfig(filename='awslog.log',level=logging.WARNING)

client = boto3.client('s3')
tmp = client.list_buckets()
buckets = tmp['Buckets']
openBuckets = set()
openObjects = set()

def auditor(event, context):
    for b in buckets:
        name = b.get('Name')
        tmp = client.get_bucket_location(Bucket=name)
        if tmp['LocationConstraint'] == None:
            region = ""
        else:
            region = "." + tmp['LocationConstraint']
        url = 'https://' + name + ".s3" + region + ".amazonaws.com/"
        logging.info(f'Checking {name} at {url}')
        try:
            request = requests.get(url)
        except Exception as e:
            logging.debug("Issue with request")
            logging.debug(e)
        if str(request.status_code)[0] == '2':
            logging.warning(f"Violation! Open bucket {name} at {url}")
            openBuckets.add(name)
            continue
        tmp = client.list_objects(Bucket=name)
        for x in tmp.get('Contents', []):
            keyURL = url + x.get('Key')
            try: 
                request = requests.get(keyURL)
            except Exception as e:
                logging.debug('Bad Object Request')
                logging.debug(e)
            if str(request.status_code)[0] == '2':
                openBuckets.add(name)
                logging.warning(f"Violation! Object {keyURL} is public")
                openObjects.add(x.get('Key'))

    return list(openBuckets)
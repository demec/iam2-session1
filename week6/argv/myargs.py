import sys


def parseArgs(args_list: list):
    if not args_list:
        return {}
    myEnuArgs = list(enumerate(args_list[1:]))
    argDict = {}
    for index, arg in myEnuArgs:
        if arg[0] == '-':
            cond = arg[1]
            if index + 1 != len(myEnuArgs) and \
               myEnuArgs[index + 1][1][0] != "-":
                value = myEnuArgs[index + 1][1]
            else:
                value = True
            argDict.update({cond: value})
    print (argDict)
    return argDict

"""
Schemea:
-v verbose (true if present, false if not)
-p port number (12345 if ommited)
-s string for source data ('./' if ommited)
-g comma seperated string for list of groups (none if ommited)
"""

def parseArgs2(args_list: list):
    if not args_list:
        return {}
    args_list.pop(0)
    argDict = {
        "Verbose": False, 
        "Port": 12345, 
        "Source": "./",
        "Groups": None
        }

    while len(args_list) >= 1:
        cond = ""
        currentItem = args_list.pop(0)
        if currentItem[0] == "-":
            cond = currentItem[1]
        if cond == 'v':
            argDict.update({"Verbose": True})
        elif cond == 'p':
            try:
                value = int(args_list.pop(0))
                argDict.update({"Port": value})
            except ValueError:
                print("Invalid Port")
            except IndexError:
                print("Not enough parameters")
        elif cond == 's':
            try:
                argDict.update({"Source": args_list.pop(0)})
            except IndexError:
                print("Not enough parameters")
        elif cond == 'g':
            try:
                argDict.update({"Groups": args_list.pop(0).split(",")})
            except IndexError:
                print("Not enough parameters")
        else:
            print("Ignoring unknown prameter", currentItem)
    print (argDict)
    return argDict



if __name__ == "__main__":
    # args = parseArgs(sys.argv)
    args = parseArgs2(sys.argv)

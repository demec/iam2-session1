import unittest
import myargs


class TestMyArgs(unittest.TestCase):

    def test_no_args(self):
        expected = {}
        result = myargs.parseArgs2([])
        self.assertEqual(expected, result)

import unittest
import bowling


class TestBowling(unittest.TestCase):

    def test_bowling_0(self):
        expected = 0
        result = bowling.calculateScore("")
        self.assertEqual(expected, result)

    def test_two_every_roll(self):
        expected = 40
        result = bowling.calculateScore("22 22 22 22 22 22 22 22 22 22")
        self.assertEqual(expected, result)

    def test_gutters(self):
        expected = 0
        result = bowling.calculateScore("00 00 00 00 00 00 00 00 00 00")
        self.assertEqual(expected, result)

    def test_spare(self):
        expected = 48
        result = bowling.calculateScore("22 2/ 22 22 22 22 22 22 22 22")
        self.assertEqual(expected, result)

    def test_misses(self):
        expected = 90
        result = bowling.calculateScore("9- 9- 9- 9- 9- 9- 9- 9- 9- 9-")
        self.assertEqual(expected, result)

    def test_all_spares(self):
        expected = 150
        result = bowling.calculateScore("5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5")
        self.assertEqual(expected, result)

    def test_strikes(self):
        expected = 50
        result = bowling.calculateScore("22 X 22 22 22 22 22 22 22 22")
        self.assertEqual(expected, result)

    def test_all_strikes(self):
        expected = 300
        result = bowling.calculateScore("X X X X X X X X X X X X")
        self.assertEqual(expected, result)

    def test_random(self):
        expected = 180
        result = bowling.calculateScore("12 4/ 4/ X X X X 7/ 22 9/ X")
        self.assertEqual(expected, result)

    def test_comb_last_frame(self):
        expected = 180
        result = bowling.calculateScore("12 4/ 4/ X X X X 7/ 22 9/X")
        self.assertEqual(expected, result)

    def test_bad_roll(self):
        self.assertEqual((21 + 14 + 4 + 7 * 30), bowling.calculateScore("X X 13 X X X X X X X X X")) # noqa

    def test_all_gutters(self):
        self.assertEqual(0, bowling.calculateScore("-- -- -- -- -- -- -- -- -- -- -- --")) # noqa

    def test_one_pin(self):
        self.assertEqual(1, bowling.calculateScore("-- -- -- -- -- -- -- -1 -- -- -- --")) # noqa

    def test_two_frames(self):
        self.assertEqual(2, bowling.calculateScore("-- -- -- 1- -- -- -- -- -- -- -1 --")) # noqa

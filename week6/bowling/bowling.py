#! /usr/bin/env python3
import sys


def calculateScore(bowling_frames_string):
    if bowling_frames_string == "":
        return 0
    framesList = bowling_frames_string.replace("-", "0").split()
    score = 0
    mustBreak = False
    for index, frame in enumerate(framesList):
        if frame == "X":
            frameScore = 10
            if framesList[index + 1] == "X":
                frameScore += 10
                if framesList[index + 2] == "X":
                    frameScore += 10
                else:
                    frameScore += int(framesList[index + 2][0])
            elif framesList[index + 1][1] == "/":
                frameScore += 10
            else:
                frameScore += int(framesList[index + 1][0]) + int(framesList[index + 1][1]) # noqa
            if index == 9:
                mustBreak = True
        elif frame[1] == "/":
            frameScore = 10
            if index == 9:
                if len(frame) == 3:
                    next = frame[2]
                elif len(framesList) == 11:
                    next = framesList[10]
                    mustBreak = True
            else:
                next = framesList[index + 1]
            if next == "X":
                frameScore += 10
            else:
                frameScore += int(next[0])
        else:
            frameScore = (int(frame[0]) + int(frame[1]))

        score += frameScore
        if mustBreak:
            break

    return score


if __name__ == "__main__":
    # call the script with an argument
    # bowling.py "5/ X X X X X X X X X X X"
    try:
        print(calculateScore(sys.argv[1]))
    except IndexError:
        print("Nothing to score")

#! /usr/bin/env python3


def fizzOrBuzz(number):
    if number % 3 == 0:
        if number % 5 == 0:
            return "FizzBuzz"
        return "Fizz"
    elif number % 5 == 0:
        return "Buzz"
    return "Nope"


if __name__ == "__main__":
    print("Yep")

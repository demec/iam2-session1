import unittest
import fizzbuzz


class TestFizzBuzz(unittest.TestCase):

    def test_fizzbuzz(self):
        expected = "FizzBuzz"
        result = fizzbuzz.fizzOrBuzz(15)
        self.assertEqual(expected, result)

    def test_fizz(self):
        expected = "Fizz"
        result = fizzbuzz.fizzOrBuzz(12)
        self.assertEqual(expected, result)

    def test_buzz(self):
        expected = "Buzz"
        result = fizzbuzz.fizzOrBuzz(10)
        self.assertEqual(expected, result)

    def test_none(self):
        expected = "Nope"
        result = fizzbuzz.fizzOrBuzz(11)
        self.assertEqual(expected, result)

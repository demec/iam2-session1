""" Read in a list of numbers (e.g., 12 4 9 18 7 13 1 5 19 ) followed by a target sum (e.g., 50)
and determine how many of the numbers are needed to exceed that sum (in this case,
you'd need 6, i.e., 12 + 4 + 9 + 18 + 7 + 13 = 63)
    • same problem, but find the smallest set of numbers from the original list whose sum
    exceeds the target (in this case the answer is 4, i.e., 19 + 18 + 13 + 12 = 62) """

#numList = input("Enter a list of numbers: ").split(' ')
numList = "12 4 9 18 7 13 1 5 19".split(' ')
#targetSum = int(input("Enter a target sum: "))
targetSum = 50
total = 0
needed = 0
while total <= targetSum:
    total += int(numList[needed])
    needed += 1

print(needed)


numList.sort(key=int, reverse=True)
#targetSum = int(input("Enter a target sum: "))
total = 0
needed = 0
while total <= targetSum:
    total += int(numList[needed])
    needed += 1    

print(needed)
#Chutes and Ladders
#Selfpaced week 4

import random

LADDER_MAP = {
    1: 38,
    4: 14,
    9: 31,
    21: 42,
    28: 84,
    36: 44,
    51: 67,
    71: 91,
    80: 100 
}
CHUTE_MAP = {
    98: 78,
    95: 75,
    93: 73,
    87: 24,
    62: 19,
    64: 60,
    56: 53,
    47: 26,
    49: 11,
    16: 6
}

def diceRoll():
    return random.randint(1,6)

#Advance Player
def advancePlayer(myPlayer, myLoc):
    if input("Player " + str(myPlayer) + " at space " + str(myLoc) + ", press enter to roll dice ('quit' to exit) ") == 'quit':
        print("Fine, I see how it is...virtual equilviant of flipping the board and storming off...")
        return -1
    myRoll = diceRoll()
    print('You rolled a ', myRoll)
    newLoc = myLoc + myRoll
    if newLoc > 100:
        print('Nope, you have to go back')
        newLoc = myLoc
    elif newLoc in CHUTE_MAP.keys():
        newLoc = CHUTE_MAP[newLoc]
        print("Oh no! You hit a chute, heading down to space", newLoc)
    elif newLoc in LADDER_MAP.keys():
        newLoc = LADDER_MAP[newLoc]
        print("YAY! You landed on a ladder, heading up to space", newLoc)
    if newLoc == 100:
        print('Player', str(myPlayer), 'you WIN!!!!')
        return -1
    print('Player', myPlayer, 'is on space', newLoc, '\n---------------------------')
    return newLoc


####Start####

player1Loc = 0
player2Loc = 0

while player2Loc >= 0:
    player1Loc = advancePlayer(1, player1Loc)
    if player1Loc == -1:
        break
    player2Loc = advancePlayer(2, player2Loc)
    
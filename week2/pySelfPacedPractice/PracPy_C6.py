#https://www.practicepython.org/exercise/2014/03/12/06-string-lists.html

def palindrome(myStr):
    origStr = myStr
    myStr = myStr.strip().lower().replace(' ', '')
    backStr = myStr[::-1]
    if myStr == '':
        return "Empty String"
    elif myStr == backStr:
        return origStr + " is a palindrome"
    else:
        return origStr + " is not a palindrome"

strInput = input("Enter a string: ")

print(palindrome(strInput))


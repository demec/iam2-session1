#https://www.practicepython.org/exercise/2014/05/15/14-list-remove-duplicates.html
#List Remove Duplicates 

def remove_via_loop(myList):
    newList = []
    for x in myList:
        if x not in newList:
            newList.append(x)
    return newList

def remove_via_set(mylist):
    return list(set(mylist))

duplist = list('abcaabbcd')
print(remove_via_loop(duplist))
print(remove_via_set(duplist))
#Rock Paper Scissiors
#https://www.practicepython.org/exercise/2014/03/26/08-rock-paper-scissors.html

#1 = rock; beats 3
#2 = paper; beats 1
#3 = scissors; beats 2

PLAY_OPTIONS = {'rock': 1, 'paper' : 2, 'scissors' : 3}
##another way to get the num to string as a comprehension, will do a dictionary inverted in this case
PLAY_NUM_TO_STR = {num: string for string, num in PLAY_OPTIONS.items()}

import random

def gen_computer():
    return random.randint(1,3)
    
def check_win(uPlay, cPlay):
    if PLAY_OPTIONS[uPlay] == cPlay:
        print('Tie!')
    elif ((PLAY_OPTIONS[uPlay] == 1 and cPlay == 3) or 
            (PLAY_OPTIONS[uPlay] == 2 and cPlay == 1) or 
            (PLAY_OPTIONS[uPlay] == 3 and cPlay == 2)):
        print('You Win!')
    else:
        print('Computer Wins!')

def gameplay():
    uPlay = input('Enter a play: ').lower()
    cPlay = gen_computer()
    if uPlay == 'quit':
        return False
    try:
        PLAY_OPTIONS[uPlay]
    except KeyError:
        print('Enter a valid option ("quit" to exit)')
        return True
    #get the computer play from the dictionary
    print('Computer plays', list(PLAY_OPTIONS)[list(PLAY_OPTIONS.values()).index(cPlay)])
    #another way to get this
    #this is a comprehension
    key = next(key for key, value in PLAY_OPTIONS.items() if value == cPlay)
    print('Computer plays', key)
    #yet another way using dict comprehension
    print('Computer plays', PLAY_NUM_TO_STR[cPlay], 'against your', uPlay)
    check_win(uPlay, cPlay)
    return True


####### START #######
print('Rock! Paper! Scissors!')
print('"quit" to exit')
a = True
while a:
    a = gameplay()
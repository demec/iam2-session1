import quorachallenge
@quorachallenge.autotest('run_length_encoder')
def my_segments(myStr):
    previous_char = ''
    segments = ""
    for myChar in myStr:
        if myChar != previous_char or len(segments) < 2:
            segments += myChar        
        else:
            a, b = list(segments[-2:])
            if str(b).isnumeric():
                ct = int(segments[-1]) + 1
                if ct > 9 :
                    segments = segments + myChar
                else:
                    segments = segments[:-1] + str(ct)
            elif a == b:
                segments = segments[:-1] + '3'
            else:
                segments += myChar
        previous_char = myChar
    return segments
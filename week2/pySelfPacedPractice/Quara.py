#https://www.quora.com/q/python-beginners/Introducing-the-First-Python-Beginners-programming-challenge
import quorachallenge
@quorachallenge.autotest('segment_counter')
def my_segments(myStr):
    previous_char = ''
    segmentCT = 0
    for myChar in myStr:
        if myChar != previous_char:
            segmentCT += 1
        previous_char = myChar
    return segmentCT
import quorachallenge
@quorachallenge.autotest('sequence_and_count')
def my_segments(myStr):
    if myStr == "":
        return [[None, 0]]
    previous_char = ''
    segments = []
    segmentCT = 0
    for myChar in myStr:
        if myChar != previous_char:
            segments.append([myChar, 1])
        else:
            segments[-1][1] += 1
        previous_char = myChar
    return segments
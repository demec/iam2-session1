#https://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html

import random
def randList():
    s = set()
    for _ in range(1, random.randint(20,100)):
        s.add(random.randint(1, 99))
    return s

a = randList()
b = randList()

print(a)
print(b)

print(a.intersection(b))
c = []
for x in a:
    if x in b:
      c.append(x)
      
print(c)
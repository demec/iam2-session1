#! /usr/bin/env python3
"""Weather.py gets weather for your current IP location

--time takes a Unix time
"""

import sys
import argparse
import requests

DARK_SKY_SECRET_KEY = "d21b17405d692b8977dd9098c59754eb"


def get_location():
    """ Returns the longitude, latitude and city for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    str: city
    """
    response = requests.get("https://ipvigilante.com/")
    res_json = response.json()
    lat = res_json['data']['latitude']
    lon = res_json['data']['longitude']
    lCity = res_json['data']['city_name']

    return lon, lat, lCity


def get_temperature(longitude, latitude, time):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):
    time (int):

    Returns:
    float: temperature
    float: temp_hich
    float: temp_low
    float: humidity
    float: feels_like
    """
    reqstr = 'https://api.darksky.net/forecast/' + DARK_SKY_SECRET_KEY + '/'
    reqstr += latitude + ',' + longitude
    if time:
        reqstr += "," + str(time)
    try:
        response = requests.get(reqstr)
        if response.status_code != 200:
            raise Exception()
    except Exception:
        sys.stderr.write("***Error accessing api***\n")
        sys.exit()
    res_json = response.json()
    try:
        gTemp = res_json['currently']['temperature']
        gLow = res_json['daily']['data'][0]['temperatureMin']
        gHigh = res_json['daily']['data'][0]['temperatureMax']
        gHumidity = res_json['daily']['data'][0]['humidity']
        gFeelsLike = res_json['currently']['apparentTemperature']
    except KeyError:
        sys.stderr.write("***Error: API did not return expceted values***")
        sys.exit()

    return gTemp, gHigh, gLow, gHumidity, gFeelsLike


def print_forecast(temp, *args):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print('The weather in: ' + args[0])
    print("Today's forecast is: " + str(temp) + " degrees! \n")
    print('High: ' + str(args[1]))
    print('Low: ' + str(args[2]))
    print('Humidity: ' + str(args[3]))
    print('Feels Like: ' + str(args[4]))


if __name__ == "__main__":
    time = None
    parser = argparse.ArgumentParser()
    parser.add_argument("--time")
    my_args = parser.parse_args()
    if my_args.time:
        try:
            int(my_args.time)
            time = my_args.time
        except ValueError:
            sys.stderr.write("Time not formated properly. Please use a Unix time code\n") # noqa
            sys.exit()
    longitude, latitude, city = get_location()
    temp, high, low, humidity, feels_like = get_temperature(longitude, latitude, time) # noqa
    print_forecast(temp, city, high, low, humidity, feels_like)

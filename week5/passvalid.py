def passvalid(password):
    strPassword = str(password)
    if strPassword == '':
        return False
    elif len(strPassword) in range (8, 13):
        if checkLetter(strPassword) and checkNum(strPassword):
            return True
        else:
            return False
    else:
        return False

def checkLetter(password):
    for x in password:
        if str(x).isalpha():
            return True
    return False

def checkNum(password):
    for num in range(10):
        if str(num) in password:
            return True
    return False
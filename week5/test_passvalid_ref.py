import unittest
import passvalid_ref as passvalid

VALID_PASSWORD = "1ValidPass!"


class TestPasswords_ref(unittest.TestCase):    
    def test_bool(self):
        expected = bool
        result = type(passvalid.passvalid(VALID_PASSWORD))
        self.assertEqual(expected, result)

    def test_blank(self):
        expected = False
        result = passvalid.passvalid('')
        self.assertEqual(expected, result)

    def test_lenght_pass(self):
        expected = True
        result = passvalid.passvalid(VALID_PASSWORD)
        self.assertEqual(expected, result)
    
    def test_min_lenght_fail(self):
        expected = False
        result = passvalid.passvalid("shrt1!")
        self.assertEqual(expected, result)

    def test_max_length(self):
        expected = False
        result = passvalid.passvalid('thisistoolongtouse1!')
        self.assertEqual(expected, result)

    def test_digit(self):
        expected = True
        result = passvalid.passvalid('password1!')
        self.assertEqual(expected, result)

    def test_digit_fail(self):
        expected = False
        result = passvalid.passvalid('password')
        self.assertEqual(expected, result)

    def test_letter_fail(self):
        expected = False
        result = passvalid.passvalid('1111111111')
        self.assertEqual(expected, result)

    def test_int(self):
        expected = False
        result = passvalid.passvalid(1111111111)
        self.assertEqual(expected, result)
    
    def test_special_fail(self):
        expected = False
        result = passvalid.passvalid("thisPasswo1")
        self.assertEqual(expected, result)
    
    def test_good(self):
        expected = True
        result = passvalid.passvalid(VALID_PASSWORD)
        self.assertEqual(expected, result)
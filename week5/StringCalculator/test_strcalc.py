import unittest
import strcalc

class TestStringCalc(unittest.TestCase):

    def test_empty_string(self):
        expected = "0"
        result = strcalc.strcalc("")
        self.assertEqual(expected, result)

    def test_single_string(self):
        expected = "3"
        result = strcalc.strcalc("3")
        self.assertEqual(expected, result)
    
    def test_two_add(self):
        expected = "4"
        result = strcalc.strcalc("2,2")
        self.assertEqual(expected, result)
    
    def test_float_add(self):
        expected = "4.2"
        result = strcalc.strcalc("2,2.2")
        self.assertEqual(expected, result)

    def test_multi_add(self):
        expected = "34"
        result = strcalc.strcalc("2,2,1,3,4,8,9,5")
        self.assertEqual(expected, result)
    
    def test_word_one(self):
        expected = "ERROR: ValueError"
        result = strcalc.strcalc("2,2,one,3,4,8,9,5")
        self.assertEqual(expected, result)

    def test_new_line_seperator(self):
        expected = "6"
        result = strcalc.strcalc("1\n2,3")
        self.assertEqual(expected, result)

    def test_multi_seperator(self):
        expected = "Error: too many seperators"
        result = strcalc.strcalc("175.2,\n35")
        self.assertEqual(expected, result)

    def test_custom_seperator(self):
        expected = "3"
        result = strcalc.strcalc("//:\n1:2")
        self.assertEqual(expected, result)

    def test_custom_seperator_multiple(self):
        expected = "3"
        result = strcalc.strcalc("//sep\n1sep2")
        self.assertEqual(expected, result)

    def test_negitive_num(self):
        expected = "Negitive not allowed"
        result = strcalc.strcalc("-1, -2")
        self.assertEqual(expected, result)
import unittest
import passvalid

class TestPasswords(unittest.TestCase):

    def test_bool(self):
        expected = bool
        result = type(passvalid.passvalid('password1'))
        self.assertEqual(expected, result)

    def test_blank(self):
        expected = False
        result = passvalid.passvalid('')
        self.assertEqual(expected, result)

    def test_lenght_pass(self):
        expected = True
        result = passvalid.passvalid("testpasses1")
        self.assertEqual(expected, result)
    
    def test_min_lenght_fail(self):
        expected = False
        result = passvalid.passvalid("short")
        self.assertEqual(expected, result)

    def test_max_length(self):
        expected = False
        result = passvalid.passvalid('thisistoolongtouse')
        self.assertEqual(expected, result)

    def test_digit(self):
        expected = True
        result = passvalid.passvalid('password1')
        self.assertEqual(expected, result)

    def test_digit_fail(self):
        expected = False
        result = passvalid.passvalid('password')
        self.assertEqual(expected, result)

    def test_letter_fail(self):
        expected = False
        result = passvalid.passvalid('1111111111')
        self.assertEqual(expected, result)

    def test_int(self):
        expected = False
        result = passvalid.passvalid(1111111111)
        self.assertEqual(expected, result)
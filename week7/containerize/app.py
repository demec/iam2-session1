import os
import sys
from flask import Flask, redirect, request, url_for
import logging

logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG, handlers=[logging.StreamHandler(sys.stdout)])


signatures = []

app = Flask(__name__)

# configuration

font = os.environ['GB_DISPLAY_FONT']
font_color = os.environ['GB_DISPLAY_COLOR'] 
environment = os.environ['GB_RUN_ENVIROMENT']

@app.route('/', methods=['GET'])
def index():
    html = """
    Signatures: <br />
    <font face="%(font)s" color="%(color)s">
        %(messages)s
    </font>

    <br /> <br />
    <form action="/signatures" method="post">
        Sign the Guestbook: <input type="text" name="message"><br>
        <input type="submit" value="Sign">
    </form>

    <br />
    <br />
    Debug Info: <br />
    ENVIRONMENT is %(environment)s
    """
    messages_html = "<br />".join(signatures)
    return html % {"font": font, "color": font_color, "messages": messages_html, "environment": environment}

@app.route('/signatures', methods=['POST'])
def write():
    message = request.form.get('message')
    signatures.append(message)

    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8088)
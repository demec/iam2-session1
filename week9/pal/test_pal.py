import unittest
import palindrome


class TestPal(unittest.TestCase):

    def test_empty_string(self):
        self.assertFalse(palindrome.is_palindrome(""))

    def test_is_not_palindrome(self):
        self.assertFalse(palindrome.is_palindrome("false"))

    def test_is_palindrome(self):
        self.assertTrue(palindrome.is_palindrome("racecar"))

    def test_is_palindrome_space(self):
        self.assertTrue(palindrome.is_palindrome("race car"))

    def test_is_palindrome_punctuation(self):
        self.assertTrue(palindrome.is_palindrome("race car!"))

    def test_is_palindrome_phrase(self):
        self.assertTrue(palindrome.is_palindrome("Mr. Owl ate my metal worm"))

    def test_is_palindrome_phrase2(self):
        self.assertTrue(palindrome.is_palindrome("Go hang a salami, I'm a lasagna hog")) # NOQA

    def test_is_palindrome_number(self):
        self.assertTrue(palindrome.is_palindrome("1Mr. Owl ate my metal worm1")) # NOQA

    def test_is_palindrome_allPunctuation(self):
        self.assertFalse(palindrome.is_palindrome("!!<<!!"))

    def test_is_palindrome_tab(self):
        self.assertTrue(palindrome.is_palindrome("Mr. Owl ate my    metal worm")) # NOQA

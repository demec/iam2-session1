import string


def is_palindrome(input_str):
    input_str = input_str.translate(str.maketrans('', '', string.punctuation))
    input_str = input_str.replace(" ", "")
    input_str = input_str.lower()
    if input_str[::-1] == input_str and input_str != "":
        return True
    return False

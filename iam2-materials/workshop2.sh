#!/bin/bash
if [ ! "$1" ]; then
	for file in `ls -lR ./ |grep -P "^\." | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi
	done
else

	for file in `ls -lR "$1" |grep -P "^\-" | awk {'print $10'}`; do
		if file -s $file | grep -P "\bimage\b"; then
			echo "$file is an image"
		fi
	done
fi
